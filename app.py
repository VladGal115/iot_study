from flask import Flask, render_template, request, redirect, url_for
import json

app = Flask(__name__)


class Sensor:
    def __init__(self, sensor_id, sensor_type, real_value):
        self.id = sensor_id
        self.type = sensor_type
        self.real_value = real_value

    def get_real_value(self):
        return self.real_value


class Room:
    def __init__(self, name, desired_temperature, light, sensor_ids):
        self.name = name
        self.desired_temperature = desired_temperature
        self.light = light
        self.sensors = self.load_sensors(sensor_ids)

    def load_sensors(self, sensor_ids):
        with open('sensors.json', 'r') as file:
            sensors_data = json.load(file)
        sensors = []
        for sensor_id in sensor_ids:
            sensor_data = next((s for s in sensors_data if s['id'] == sensor_id), None)
            if sensor_data:
                sensors.append(Sensor(sensor_data['id'], sensor_data['type'], sensor_data['real_value']))
        return sensors


def load_rooms():
    with open('rooms.json', 'r') as file:
        rooms_data = json.load(file)
    rooms = []
    for room_data in rooms_data:
        rooms.append(
            Room(room_data['name'], room_data['desired_temperature'], room_data['light'], room_data['sensors']))
    return rooms


def save_rooms(rooms):
    with open('rooms.json', 'w') as file:
        rooms_data = [{
            'name': room.name,
            'desired_temperature': room.desired_temperature,
            'light': room.light,
            'sensors': [sensor.id for sensor in room.sensors]
        } for room in rooms]
        json.dump(rooms_data, file, indent=4)


@app.route('/')
def index():
    rooms = load_rooms()
    return render_template('index.html', rooms=rooms)


@app.route('/room/<room_name>', methods=['GET', 'POST'])
def room(room_name):
    rooms = load_rooms()
    room = next((r for r in rooms if r.name == room_name), None)
    if room is None:
        return "Room not found", 404

    if request.method == 'POST':
        room.desired_temperature = int(request.form['desired_temperature'])
        room.light = request.form['light']
        save_rooms(rooms)
        return redirect(url_for('index'))

    return render_template('room.html', room=room)


@app.route('/add_room', methods=['GET', 'POST'])
def add_room():
    if request.method == 'POST':
        room_name = request.form['name']
        desired_temperature = int(request.form['desired_temperature'])
        light = request.form['light']
        sensor_ids = request.form.getlist('sensors')

        rooms = load_rooms()
        new_room = Room(room_name, desired_temperature, light, sensor_ids)
        rooms.append(new_room)
        save_rooms(rooms)
        return redirect(url_for('index'))

    with open('sensors.json', 'r') as file:
        sensors = json.load(file)
    return render_template('add_room.html', sensors=sensors)


@app.route('/delete_room/<room_name>')
def delete_room(room_name):
    rooms = load_rooms()
    rooms = [room for room in rooms if room.name != room_name]
    save_rooms(rooms)
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)
